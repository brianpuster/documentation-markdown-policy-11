title: GC Sales PIP  
published: 2019-09-05  
author: Jennifer Manguiat  
updated: 2020-12-04   


# Performance Improvement Plan Process

Sales Consultants’ performance will be reviewed by Sales Management on a monthly basis at the beginning of each month for performance against goal.  

Metric
- Monthly 1st year realized revenue Goal (ACV Quota  

# Orientation Period Performance Guidelines – 0-6 month tenured Sales Representative

- Satisfactory attendance - outlined in the Employment Handbook
- Attitude that is open coaching and feedback
- Meeting or exceeding the training metrics as defined within the offer letter by management
- Completion of all required Certifications

*Upon successful completion of the orientation period team members will enter the “regular” employment classification*  

*If team members are unable to demonstrate their ability to achieve a satisfactory level of performance during their orientation period their employment could be terminated.*  

# Minimum Performance Requirements

- 7-11 month tenured Sales Representative  
    - If the 90 day rolling Performance to Goal is below 60%, the Sales Rep. must be meeting or exceeding a monthly Performance to Goal of 70%, unless the Sales Rep. has had the prior two contiguous months at less than 50% of Goal.  
    - All sales Representatives must maintain an average of 22 calls per day AND at least a 1:15 average talk time per day, measured weekly.  
- 12+ months tenured Sales Representative  
    - If the 90 day rolling Performance to Goal is below 70%, the Sales Rep. must be meeting or exceeding a monthly Performance to Goal of 80%, unless the Sales Rep. has had the prior two contiguous months at less than 50% of Goal.  
    - All sales Representatives must maintain an average of 22 calls per day AND at least a 1:15 average talk time per day, measured weekly.  
    
If performance requirements as stated above are not met, the sales rep will be placed on a Performance Improvement Plan (PIP) at the beginning of the month. In order to be removed from the PIP, the Performance requirements as stated above must be achieved.

# Performance Improvement Process Post Orientation Period  

- 1st Incident – Verbal Coaching
- 2nd Incident – 1st Stage Improvement Plan (weekly meetings with manager to discuss progress)
- 3rd Incident – 2nd Stage Improvement Plan (weekly meetings with manager to discuss progress)
- 4th Incident – Termination

# Policy Changes

Management reserves the right to by-pass stages of the process outlined above depending on the nature of the issue.  

Minimum monthly threshhold as it pertains to the performance plan will be pro-rated for approved FMLA or bereavement absences.  

Sales Consultants’ with planned time off (PTO time) scheduled and approved at least a month in advance for a minimum of 3 consecutive days will only be responsible for the pro-rata portion of the goal associated with the number of business days worked.  

This is a guideline for a performance improvement plan and is subject to change at the discretion of the GM, CRO or CPO of ContructConnect.  

# Acknowledgement

I have read and acknowledge the GC Sales Performance Improvement Plan Process to be the process I will be held accountable to regarding my performance.
