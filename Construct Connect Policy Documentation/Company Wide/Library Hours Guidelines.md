title: Library Hours Guidelines  
author: Buck Brody  
published: 2021-02-26  
effective date:  2021-03-01  



The company is committed to a work environment that allows our team members to perform their role with excellence.  This includes having time to focus on important aspects of the role without distraction.  Library hours allow this opportunity. 

# Company Guidelines  
- Library hours for most teams are during the hours of 1-3 pm on Tuesdays and Thursdays.  
- No internal meetings should be scheduled or accepted during these hours to allow for focus.  
- The Company reserves the right to suspend library hours at any time and for any reason.  

# Team Guidelines  
- The Purpose   
    - This is time to do focused work on the important but not urgent work that is otherwise hard to get to  
    - May or may not include training / skill development  
- The Rules  
    - Should be distraction free  
    - No talking, phone calls or use of MS Teams  
    - Sending and checking emails is to be avoided  
    - Exceptions: We have certain roles that have internal 'customers' that will have to make exceptions. Please discuss with your manager.  
- Additional Background Materials  
    - http://www.paulgraham.com/makersschedule.html  
    - https://www.goodreads.com/book/show/28383248-deep-work   
- All teams should be aware of and honor the library hours of other teams.  

# Library Hours By Team  
- Product Management – Tuesday and Thursday 1-3 pm EST  
- Product Development – Friday 1-3 pm EST  
- Content – 1 hour a week on Tuesday or Thursday, 1-3 pm EST  
- General Contractor Sales and Support - Overall - GC Team not participating, any individual who is will block their calendar  
- Trade Contractor, Takeoff, Upsell Sales and Customer Success -Not participating  
- BPM Sales and Customer Success – Not participating  
- Finance – Tuesday and Thursday 1-3 pm EST  
- Canada – 2 hours per week, those who participate will do so within the Tuesday/Thursday hours  
- People & Culture – Thursday 1-3 pm EST  
