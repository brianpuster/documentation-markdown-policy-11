title: Renewal Price Uplift Policy    
published: 2020-12-18    
author: Jeff Cryder      


# Policy Level
Very Important  

# Approvers  
- Dave Storer, VP Controller 
- Jim Hill, EVP General Manager Trade Contractors 
- Howard Atkins, EVP General Manager Building Product Manufacturers 
- Jon Kost, EVP General Manager General Contractors 

# Purpose

The purpose of this policy is to manage the Company's price uplift rules that are applied to all expiring contracts. The application of these rules results in quoted renewal pricing for an upcoming renewal term.  Renewal quotes are reviewed by assigned CSMs prior to submission to the expiring subscriber for consideration.

# Policy

[Renewal Price Uplift Policy](https://constructionmd.sharepoint.com/:w:/r/sites/RenewalPriceUpliftPolicyTeam/_layouts/15/Doc.aspx?sourcedoc=%7BB5B2F02F-992F-417C-BE3C-0C45A264EE6C%7D&file=Renewal%20Price%20Uplift%20Policy.docx&action=default&mobileredirect=true)  

If you do not have permission to open this document, please contact <jeff.cryder@constructconnect.com> or <dave.storer@constructconnect.com>
