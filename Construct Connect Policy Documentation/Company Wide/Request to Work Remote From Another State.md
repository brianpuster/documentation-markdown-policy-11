title: Request to Relocate to Another City/State    
author: Monica Brooksbank   
published: 2019-09-27  
effective date: 2019-09-27    
policy level: Important Not Urgent    
approver(s): Senior Finance, VP People & Culture  
applicable locations: All  
updated: 2020-12-03  

# GUIDELINES:  
Requests to relocate to another city/state will be reviewed and approved in accordance with the following guidelines.  

- Relocation requests must be submitted at least 30 days prior to the relocation  
- Details of the new location and the reason for the request should be outlined on this form below  
- Certain states may not be eligible for relocation due to business reasons  
- Requests must be approved by the team member’s manager and Payroll prior to the move  
- A Telecommuting agreement should be completed and signed if relocation creates a new remote working arrangement  
- New or additional company equipment will not be issued due to the relocation  
- The company will not reimburse for moving expenses; including time off to move, temporary living expenses, relocation of household goods or miscellaneous associated expenses.  

## Request Form
---
To be completed by the Team Member:  

Name:  
Job Title:  
Team:   
Manager:  
Relocation City & State:   
Reason for relocation:  
---
## Approvals
---
Team Leadership Approval:
Team Member Manager:  
---
Payroll Review:  
☐  Approved  
☐  Denied  

Reason for denial if applicable:   
Payroll Approver: 
---