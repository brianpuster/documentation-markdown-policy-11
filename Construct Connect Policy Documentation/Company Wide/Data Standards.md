title: Data Standards Policy  
author: Steve Testa  
published:  2020-12-10  


# Overview  

Data Standards have been established to ensure our systems can accurately identify contact and account records and match them accordingly. They are critical to providing a single view of an Account and Customer Journey across the entire organization. They enable data stewardship, marketing and sales workflows.  

# Data Standards

|Object|Field|Standard|Notes|
| --- | --- | --- | --- |
|**Account**||||
|Required|Account Name|Entity name only (No location or other information)|Evaluating data stewardship workflow|
|Required|Account Phone|(123) 456-7890|Evaluating data stewardship workflow|
|Required|Billing Address|*Address validation tooling to standardize input of Street, City, State, Zip (No free form entry)*|USPS Validation|
|||*Zuora  applies full state name using 2 digit state code and 3 digit country code*|USPS Validation|
|Required|Address Line 1|123 Main Street Ste 300|USPS Validation|
||Address Line 2||USPS Validation|
|Required|City|Full city name|USPS Validation|
|Required|State|Full State Name|USPS Validation|
|Required|Zip Code|12345-6789|USPS Validation|
|Required|Country|[ISO Standard Names and Codes](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/D_Country%2C_State%2C_and_Province_Codes/A_Country_Names_and_Their_ISO_Codes)||
|Required|US State Codes|[United States Standard State Codes](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/D_Country%2C_State%2C_and_Province_Codes/B_State_Names_and_2-Digit_Codes)||
|Required|Canadian Province Codes|[Canadian Standard Province Codes](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/D_Country%2C_State%2C_and_Province_Codes/C_Canadian_Province_Names_and_2-Digit_Codes)||
|**Contact**||||
|Required|First Name|No special characters|Evaluating data stewardship workflow|
|Required|Last Name|No special characters|Evaluating data stewardship workflow|
|Required|Phone|(123) 456-7890|Evaluating data stewardship workflow|
|Required|Mobile Phone|(123) 456-7890|Evaluating data stewardship workflow|
|Required|Email Address|Salesforce applies as lower case, validates basic formatting|Evaluating data stewardship workflow, tooling for email validation|
|Required|Mailing Address|*Address validation tooling to standardize input of Street, City, State, Zip (No free form entry)*|USPS Validation|
|||*Zuora  applies full state name using 2 digit state code and 3 digit country code*|USPS Validation|
|Required|Address Line 1|123 Main Street Ste 300|USPS Validation|
||Address Line 2||USPS Validation|
|Required|City|Full city name|USPS Validation|
|Required|State|Full State Name|USPS Validation|
|Required|Zip Code|12345-6789|USPS Validation|
|Required|Country|[ISO Standard Names and Codes](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/D_Country%2C_State%2C_and_Province_Codes/A_Country_Names_and_Their_ISO_Codes)||
|Required|US State Codes|[United States Standard State Codes](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/D_Country%2C_State%2C_and_Province_Codes/B_State_Names_and_2-Digit_Codes)||
|Required|Canadian Province Codes|[Canadian Standard Province Codes](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/D_Country%2C_State%2C_and_Province_Codes/C_Canadian_Province_Names_and_2-Digit_Codes)||
|**Lead**||||
|Required|Company Name|Entity name only (No location or other information)|Evaluating data stewardship workflow|
||First Name|Required to convert. No special characters|Evaluating data stewardship workflow|
|Required|Last Name|No special characters|Evaluating data stewardship workflow|
||Phone|Required to convert. (123) 456-7890|Evaluating data stewardship workflow|
|Required|Email Address|Salesforce applies as lower case, validates basic formatting|Evaluating data stewardship workflow, tooling for email validation
||Mailing Address|Required to convert. *Address validation tooling to standardize input of Street, City, State, Zip (No free form entry)*|USPS Validation|
|||*Zuora  applies full state name using 2 digit state code and 3 digit country code*|USPS Validation|
||Address Line 1|Required to convert. 123 Main Street Ste 300|USPS Validation|
||Address Line 2||USPS Validation|
||City|Required to convert.  Full city name|USPS Validation|
||State|Required to convert.  Full State Name|USPS Validation|
||Zip Code|Required to convert.  12345-6789|USPS Validation|
||Country|[ISO Standard Names and Codes](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/D_Country%2C_State%2C_and_Province_Codes/A_Country_Names_and_Their_ISO_Codes)||
||US State Codes|[United States Standard State Codes](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/D_Country%2C_State%2C_and_Province_Codes/B_State_Names_and_2-Digit_Codes)||
||Canadian Province Codes|[Canadian Standard Province Codes](https://knowledgecenter.zuora.com/BB_Introducing_Z_Business/D_Country%2C_State%2C_and_Province_Codes/C_Canadian_Province_Names_and_2-Digit_Codes)||
