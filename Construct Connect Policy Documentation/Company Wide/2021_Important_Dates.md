title: 2021 Important Dates    
published: 2020-12-18    
author: Monica Brooksbank    


# Policy Level: 
Very Important

# Distribution:  
Company-wide

# Effective date:  
2021-01-01

# Approval
Executive Leadership Team

# HOLIDAYS
|HOLIDAY|DATES| 
| --- | --- |  
|3 - Floating Holidays|Can be used any day during 2021|  
|New Years Day|Friday, January 1st|  
|Memorial Day|Monday, May 31st|  
|Independence Day|Monday, July 5th|  
|LaborDay|Monday, September 6th|  
|Thanksgiving Day|Thursday, November 25th|  
|Day after Thanksgiving|Friday, November 26th|  
|Christmas Day - Observed|Friday, December 24th|  
|New Years Day|Friday, December 31st|  

# PAY DAYS
|Month|1st Pay Date|2nd Pay Date|  
| --- | --- | --- |  
|January|January 15th|January 29th|  
|Febuary|Febuary 12th|Febuary 26th|  
|March|March 15th|March 31st|  
|April|April 15th|April 30th|  
|May|May 14th|May 28th|  
|June|June 15th|June 30th|  
|July|July 15th|July 30th|  
|August|August 15th|August 31st|  
|September|September 15th|September 30th|  
|October|October 15th|October 29th|  
|November|November 15th|November 30th|  
|December|December 15th|December 31st|  