author: Greg Schloemer  
title: Delegation of Authority Matrix - ConstructConnect 
published: 2021-02-15
Effective: January 1, 2020.

<a href="/static/media/finance/Finance/Finance_Landing_Page/Roper_Delegated_of_Authority_Matrix_Feb_1_2021_final.pdf">Roper Delegated of Authority Matrix 2.1.21</a>

ConstructConnect Delegation of Authority for internal use.

The following document sets out the delegated authority limits within the Company. These limits reflect the delegation of authority considered appropriate by the Roper Technologies and by extension, where applicable, by ConstructConnect. This document is intended to supplement the Roper Delegated Authority Matrix so Roper will apply in the instance there are any conflicts.  It is a key component of our internal controls system and, with any internal controls matter, compliance is considered to be an absolute requirement.

The positions of President, Senior Finance , Controllership, Level 2, Level 1 and HR all refer to ConstructConnect positions.  For the most recent list of individuals that meet these criteria or any questions regarding application of this matrix, please visit the Deleagtion of Authority - Names Directory.  

Transactions denominated in a currency other than U.S. dollars should be translated into U.S. dollars at a current currency exchange rate.  

All approvals must be obtained prior to commitment with an employee or third party.  Purchase requisitions should be used to facilitate these approvals in a timely manner.  Please note that purchases above set thresholds below will require review and approval by Roper executives, so please plan accordingly.  

Senior Finance represents most senior finance professional or his/her specified designee where noted with ** mark

 <br>   
  
# **Finance Related**    
 
|  **Ref #** |**Authorization Issue** |**Limit** |**Required Approval** |  
|--- |--- |--- |--- |  
|**1.00** | **Capital Expenditures** <br> All expenditures for PP&E, a project, or a series of related projects. Total cost must be > $5,000 for capital treatment. | >$50,000 <br> >$25,000 <br> All <br> All | See Roper Matrix<br> President <br> Senior Finance <br> Level 2 |
|**1.00a** | **Credit Card Administration** <br> Issuance of new cards, removing cards, setting credit limits. | All  | Controllership |
|**1.00b** | **Credit Memos** <br> Disbursements to customers but excludes processing of duplicate or overpayments (the latter subject to finance approval) | >$10,000 <br> >$2,500 <br> <=$2,500 | Senior Finance**<br> Level 2 (sales) <br> Level 1 (sales) |
|**1.01** | **Leases** <br> Accounting must review all new lease programs (auto, real estate, equipment, etc.) prior to purchase to determine <br> appropriate classification (capital lease vs operating lease). | | |
|**1.01a** | **Finance Lease Obligations** <br> The capital cost of a lease or a series of leases or series of related capital expenditures. A finance lease for real <br> estate property is subjected to the stricter approval requirements of this Section and Section 1.01b(i) | >$50,000 <br> All <br> All <br> All | See Roper Matrix<br> President <br> Senior Finance <br> Level 2 |
|**1.01b** | **Operating Leases** |  |  |  |
|**1.01b(i)** | **Real Property Leases** <br> Leases for land and / or buildings must go through a Corporate approved broker. Approvals for the actual lease <br> are required at the most restrictive level defined by either the Lease Term or Annual Payments. | All <br> All <br> All | See Roper Matrix <br> President <br> Senior Finance |
|**1.01b(ii)** | **Vehicle Leases** <br> The Roper Vehicle program defines the eligibility for company vehicles and the current selection of vehicles, <br> maximum lease payments, terms and conditions for providing a company vehicle. |  |  |
|**1.01b(iia)** | **Vehicles Driven by A Corporate Offices** | All <br> All | See Roper Matrix <br> President |
|**1.01b(iib)** | **All other vehicles within current Norms** | All <br> All <br> All | See Roper Matrix <br> President <br> Senior Finance |
|**1.01c** | **Other OPerating Leases** <br> Approval value based upon the total payments committed for the full duration of the lease (early termination <br> options shall be ignored for purposes of assessing requisite authority). Extensions require approval based <br> on the total payments committed under the extension. | >$50,000 <br> All <br> All | See Roper Matrix <br> President <br> Senior Finance |
|**1.02** | **Acquisition of other noncurrent assets** <br>  Includes purchase of intellectual property, software development projects- implementation costs for internal use software <br>  or cloud-based services such as an ERP or CRM. At inception there will likely be varying degrees of accuracy in estimates of total costs. <br> These estimates will likely require revisions over time. | >$50,000 <br> All <br> All <br> All | See Roper Matrix <br> President <br> Seniot Finance <br> Legal (Patent Related) |
|**1.03** | **Business Acquistions** <br> The limits are defined by the estimated purchase price of the acquisition costs. |  |  |
|**1.03a** | **Issuance of a Letter of Intent** | All | See Roper Matrix |
|**1.03b** | **Approval of Acquistion**  | All | See Roper Matrix |
|**1.04** | **Disposal of a Business** <br> Total anticipated proceeds require approvals as follows: | All | See Roper Matrix |
|**1.05** | **Disposal of Assets (excluding a business)** <br> Asset dispositions are to be made at fair value. Disposals with net book values require the following approvals: | >$25,000 <br> >$10,000 <br> All | See Roper Matrix <br> President <br> Senior Finance |
|**1.06** | **Transfer Pricing - Exceptions to Company Policy** <br> Roper's current policy is to sell or transfer at arms-length pricing when crossing tax-borders (based <br> on tax-advice) and cost plus 10%, otherwise. All exceptions to this policy require approval. | >$25,000 <br> >$10,000 <br> All | See Roper Matrix <br> President <br> Senior Finance |
|**1.07** | **Tax Settlement Agreements** <br> Agreements with any federal, state, municipal or other local taxing authority (income, excise, sales, etc) to settle <br> disputes regarding any taxation issue. | All | See Roper Matrix |
|**1.08** | **Approval of Expense Reports** <br> Subject to requirements of dollar thresholds in 2.00a <br> [Travel, Enterainment and Gift Policy](/pages/finance/Finance/Accounting/PurchaseToPay/Travel%20and%20Entertainment%20Policy) <br> [Concur Quick Start Guide](/pages%2Ffinance%2FFinance%2FAccounting%2FPurchaseToPay%2FConcur%20QuickStart%20Guide) | President <br> Senior Finance <br> Other Personnal | Roper Executive <br> President <br> Immediate Supervisor or Controllership |
|**1.09** | **Provision of travel/pay advance** <br> Travel advances should generally be limited to exceptional cases of hardship where an employee has very limited credit availability. | All | President <br> Senior Finance <br> HR |
|**1.10** | **Checks** <br> Signatures can only be from authorized signors per the banking resolution | >$10,000 <br> <=$10,000 | Two Signatures <br> One Signature |
|**1.10a** | **Other Disbursements**(just payment, not purchase decisions) <br> Wire Transfer / ACH Template Setup <br> Template Wire Transfers / ACH <br> Free Form Wire Transfers <br> All wire transfers | All <br> >$10,000 <br> >$10,000 <br> <$10,000 | Senior Finance <br> Two Signatures <br> Senions Finance <br> Controllership |
|**1.11** | **Authorization of Check signatories** <br> Check signatories should generally be limited to members of a location’s leadership group (e.g. Division or <br> Subsidiary President / Controllership). | All | See Roper Matrix |
|**1.12** | **Statutory Financials** <br> Approval of statutory subsidiary financials before filing is required (English translation). | All | See Roper Matrix |
|**1.13** | **IT-Q4 Financial System Implementatios** | All | See Roper Matrix |
|**1.14** | **Communications with Media and Investors** <br> Communications with media or investors (Corporate related or individual company) must be approved as follows: <br> • Media communication that is going out over a wire service – anything from a free wire service to an established wire service <br> provider (e.g. – PR Newswire).  This approval is necessary regardless of whether Roper Industries is mentioned. <br> •	Any media communication, regardless of distribution, which references Roper Industries or the ticker symbol “ROP.”  This is because <br> there are many search engine optimization services capable of picking up your media communication and distributing <br> them out as alerts on search engines. <br> • All other forms of press releases can be sent out without Roper approval. | All | See Roper Matrix |

<br>

# Contractual or Legal Related
 
|  **Ref #** |**Authorization Issue** |**Limit** |**Required Approval** |  
|--- |--- |--- |--- |  
|**2.00** | **Other Contractual Obligations**| | |
|**2.00a** | **Operating procurement contractual commitments in the normal course of business** <br> The limit is the amount that would be needed to terminate the obligation / obligations (applies to single or multiple <br> agreement entered into as a single undertaking). <br> [Consulting Policy](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Consulting%20and%20Professional%20Services%20Purchasing) <br> [Software Policy](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Software%20Purchasing) | >$1,000,000 <br> >$250,000 <br> >$25,000 <br> >$5,000 <br> <$5,000 | See Roper Matrix <br> President <br> Senior Finance** <br> Level 2 or Conrollership <br> Level 1 |
|**2.00b** | **All other Contractural commitments** <br> Includes large long-term projects to **perform** work or supply product. For clarification, large software projects that also <br> contain more than $2,000,000 of implementation services are subject to 2.00b | | Same as above |
|**2.00c** | **Commitments to but or sell in unusual currencies** <br> Commitments to buy or sell in currencies other than USD, Euro, GBP, Canadian Dollars or Yen must be approved. <br> Transactions in the functional currency of a subsidiary are excluded. | >$50,000 <br> > $25,000 <br> All | See Roper Matrix <br> President <br> Controllership |
|**2.00d** | **Consulting Agreements** <br> Work to be Performed by the Company's auditors (PWC). <br> <br> Lobbyist and lobbying activity. <br> <br> All other third-party consulting agreements outside the ordinary course (based on total contract value). And is for the <br> discrete engagement versus normal operating arrangements (such as contractors and/or employee stand-ins). <br> [Consulting Policy](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Consulting%20and%20Professional%20Services%20Purchasing) | <br> All <br> <br> All <br> <br> >$50,000 <br> >$25,000 <br> All | <br> See Roper Matrix <br> <br> See Roper Matrix <br> <br> See Roper Matrix <br> President <br> Controllership |
|**2.00e** | **Contracts with former employees (total expected value)** | >$25,000 <br> All <br> All | See Roper Matrix <br> President <br> Senior Finance
|**2.00f** | **Assumption of Legal Risk in Contractual Commitments** <br> It is Roper’s position that uncapped legal risk in contractual commitments of any kind will not be accepted. | | |
|**2.00f(i)** | **Assumption of Legal Risk in Contractual Commitments** <br> Uncapped legal risk in contractual commitments of any kind | All | President, Roper Legal |
|**2.00f(ii)** | **Direct Damages/Indirect Damages** <br> Assumption of legal risk in contractual terms (such as form T&C’s of Sale or Service) in the context of indemnification <br> or limitation of liability clauses for direct damages (i.e., bodily injury, property damage). | >$2,000,000 <br> Up to $2,000,000 | See Roper Matrix <br> President, Roper Legal |
|**2.01** | **Intellectual Property** | | |
|**2.01a** | **Licensing of Roper's Intellectual Property** <br> Roper's intellectual property is considered under executive management and as such, subsidiaries require corporate <br> approval to sell, lease or lend outside of Roper. The measure for approval is the underlying fair value of the intellectual property. | All <br> All <br> All | See Roper Matrix <br> President <br> Senior Finance |
|**2.01b** | **In-licensing of External Intellectual Property** | All | See Roper Matrix |
|**2.01c** | **Intra-group licensing of Roper's Intellectual Property** <br> Terms will be established as outlined in the corporate transfer-pricing policy. | All <br> All <br> All | See Roper Matrix <br> President <br> Senior Finance |
|**2.02** | **Debt Agreements** <br> Debt Agreements are considered indebtedness and are governed by certain lending covenants. Such agreements are <br> generally solely for Roper Corporate to enter in-to. | All <br> All <br> All | See Roper Matrix <br> President <br> Senior Finance |
|**2.03** | **Letters of Credit and Bank Guarantees** <br> Letters of credit are a utilization of borrowing capacity on a semi-permanent basis. | All <br> All <br> All | See Roper Matrix <br> President <br> Senior Finance |
|**2.04** | **Bonds (Surety, Bid, etc...)** | >$250,000 <br> >$100,000 <br> All | See Roper Matrix <br> President <br> Senior Finance |
|**2.05** | **Guarantee of Obligations (Third party of Otherwise)** <br> As a general rule, a subsidiary should not guarantee obligations of third parties in the normal course of business and <br> thus any such guarantees are considered exceptional.  Likewise, Roper generally does not provide a parent company <br> guarantee in contractual commitments except in unusual circumstances.  Requests for guarantees of any <br> kind require approval. | All <br> All <br> All | See Roper Matrix <br> President <br> Senior Finance |
|**2.06** | **Opening of new Business Locations** <br> A new location is any activity creating nexus for an existing legal entity at the state or federal level (includes employee in new state) | Physical Location <br> Physical Location <br> All | See Roper Matrix <br> President <br> Controllership, HR |
|**2.07** | **Formation of New Subsidiaries** <br> A new subsidiary is the creation of a new legal entity that will be included as part of the Company’s consolidated results. | All <br> All | See Roper Matrix <br> See Roper Matrix |
|**2.08** | **Joint Ventures or Contractual Business Combinations** | All <br> All | See Roper Matrix <br> President |
|**2.09** | **Product Liability, Warranty, or Settlements of Other Legal Disputes** <br> Excludes the replacement of product in the normal course of business. (Please note that insurance notification <br> protocols should be followed where applicable). | >$100,000 <br> All | See Roper Matrix <br> President, Roper Legal |
|**2.10** | **All Legal Activity** <br> All Legal activity, including hiring of counsel, payments to counsel, and settlement agreements. | All | All |
|**2.11** | **Product Catalog / Pricing Calculator** <br> New product launches, sunsets, pricing strategy All other configuration or amount changes. | All <br> All | President, Senior Finance <br> Controllership |
 
<br>

# Human Resources and Employee Related
 
|  **Ref #** |**Authorization Issue** |**Limit** |**Required Approval** |  
|--- |--- |--- |--- |  
|**3.00** | **Hiring of Employees** <br> [Hiring Requistion](  /pages%2Fpolicy%2FConstruct%20Connect%20Policy%20Documentation%2FCompany%20Wide%2FRequisition%20and%20Salary%20Adjustment) | Local Officers <br> Other SENIOR Staff <br> <br>  HR <br> <br> Controllership <br> <br>Positions not included in '0+12 Forecast' | See Roper Matrix <br> See Roper Matrix & President <br> See Roper Matrix & President <br> President and Senior Finance <br> Senior Finance** & Level 2
|**3.00a** | **Employment Agency Contracts** | All | Senior Finance** & HR |
|**3.00b** | **Relocation, Signing, or Other Bonus associated with the Hiring Process** | All | Senior Finance** & HR |
|**3.01** | **Employee Termination** <br> Approval of a termination includes approving the severance of employment, the financial terms of that event, the appropriate <br> use of outside counsel and the way and timing of communicating the event. | President <br> SENIOR Staff <br> <br> Reductions in Force (affecting 10% or more of a work sit or business group) <br> Level 2 <br> All Other Employees <br> Office Closures <br> | See Roper Matrix <br> See Roper Matrix & President <br> See Roper Matrix & President <br> President & HR <br> HR <br> See Roper Matrix & President |
|**3.02** | **Immigration** <br> Approval of permanent residency sponsorships (Green Card) | All | Coprorate Cpmtroller <br> Roper Legal |
|**3.03** | **Salary Increases** <br> [Salary Adjustment](/pages%2Fpolicy%2FConstruct%20Connect%20Policy%20Documentation%2FCompany%20Wide%2FRequisition%20and%20Salary%20Adjustment)| President & SENIOR Staff <br> General Salary Increase <br> Special Salary Increase | See Roper Matrix <br> Roper, President <br> Senior Finance** & HR |
|**3.04** | **Incentives or Bonus Plans for SENIOR Management (excludes sales compensation and employees below SENIOR management)** | All | See Roper Matrix & President |
|**3.05** | **Bonus Payouts to employees below SENIOR management** | Incentive or Bonus Plans of Senior Management Teams or Corporate Officers <br> Discretionary Divisional Programs <br> Commission Plans <br> Short Term Sales Incentive Plans (up to $5k/gtr) | See Roper Matrix <br> Roper, President, Senior Finance <br> HR, Level 2, Senior Finance <br> Level 2 (sales) |
|**3.05a** | **Employee Recognition Bonuses** <br> Cash and non-cash recognition aggregated at time of Purchase or within 30 days of previous request, excludes first $5k/qtr <br> requested of Short Term Sales Incentive Plans above. <br> [Gift Card Policy](/pages%2Fpolicy%2FConstruct%20Connect%20Policy%20Documentation%2FCompany%20Wide%2FGift%20Card%20Reward) | >$1,000 <br> >$75 but <$1,000 <br> <=$75 | Senior Finance** <br> Level 2 <br> Level 1 |
|**3.06** | **Amendments to Non-Roper Administered Benefits Plans** <br> Amendments include any extensions of benefits or provision of any other non-salary entitlements for non-Roper administered group <br> health and welfare and retirement benefit plans. Administrative or compliance amendments are excluded. | All | See Roper Matrix & President | 
|**3.07** | **Exceptions to Roper Administered Benefit Plan Rules and Policies** <br> Exceptions include any extensions / changes of benefits or provision of any other non-salary entitlements for Roper administered <br> group health and welfare and retirement benefit plans. | All | See Roper Matrix & President |
|**3.08** | **Ex-Patriot Employment Package** <br> Terms and conditions and structuring of package. | All | See Roper Matrix |
|**3.09** | **Worers' Compensation Cases** <br> Settlements, mediations, reserve establishment etc. | All | See Roper Matrix |
|**3.10** | **Approval of all EEO-1, VETS 100 and 100A, and Plan 5500’s  filings** | All | See Roper Matrix |
|**3.11** | **Disputes with Current or Former Employees** <br> Demands by current or former employees and/or requests for settlement agreements with employees. (Please note that insurance <br> notification protocols should be followed where applicable). | All | See Roper Matrix |
|**3.12** | **Charitable Contributions**(Aggregate annual amount) | >$10,000 <br> >$1,000 <br> <=$1,000 | President <br> Senior Finance** <br> HR |


# Approval Levels  

|  **Approval Level** |**Approval Amount**|
|---|---|
|Roper | Greater than $1M|
|President | $0 - $999,999.99|
|Senior Finance** | $0 - $249,999.99 |
|Level 2 | $0 - $24,999.99 |
|Controllership | $0 - $24,999.99 |
|Level 1 | $0 - $4,999.99 |
|Level 1* | $0 - $2,499.99 |
|HR^| |

 **Refers to Operating Expenses. Delegation of Authority Matrix takes precidence when stating lower amounts (e.g. customer refunds)  
 ^ indicates individual has HR approval  
 <br> 

# Name Directory  

|  **Name** |**Approval Authority** |**Team** |  
|--- |--- |--- |
|Conway, David| President | ISQ 700 | 
|Brody, Buck | Senior Finance | ISQ 700 | 
|Cryder, Jeff | Senior Finance** | ISQ 700 | 
|Atkins, Howard | Level 2 | ISQ640 |
|Bangs, Jim | Level 2 | ISQ610 |
|Casaletto, Mark | Level 2 | ISQ700 |
|Ciganek, Todd| Level 2 | ISQ 501 |
|Hart, Alexander | Level 2 | ISQ720 |
|Hill, Jim | Level 2 | ISQ620 |
|Johnson, Jennifer | Level 2 | ISQ735 |
|Kost, Jonathan | Level 2 | ISQ650 |
|Meske, Randall | Level 2 | ISQ620 |
|Storm, Julie^ | Level 2 | ISQ740 |
|Ven, Bob | Level 2 | ISQ730 & 720 |
|Volpenhein, Terah | Level 2 | ISQ621 |
|Messmer, Alissa | Level 2 | ISQ621 |
|Storer, David | Controllership | ISQ710 |
|Bodge, Eric | Controllership | ISQ710 |
|Misquitta, Errol | Controllership | ISQ710 |
|Meadows, Kristina | Controllership | ISQ710 |
|Barger, Crystal | Level 1 | ISQ621 |
|Bevill, Doug | Level 1 | ISQ630 |
|Borsje, Theodore | Level 1 | ISQ735 |
|Brainard, Kerry | Level 1 | ISQ700 |
|Castelli, Angelo | Level 1 | ISQ700 |
|Ciacchekka, Giovanni | Level 1 | ISQ620 |
|Collins, Gregory | Level 1 | ISQ735 |
|DiChiara, Mike | Level 1 | ISQ710 |
|Duncan, Mike | Level 1 | ISQ650 |
|Edwards, Marg | Level 1 | ISQ501 |
|Ellis, Mike | Level 1 | ISQ651 |
|Frank, Andy | Level 1 | ISQ610 |
|Gambino, Melody | Level 1 | ISQ600 |
|Gavini, Chakri | Level 1 | ISQ730 |
|Griffiths, Brian | Level 1 | ISQ622 |
|Guffey, Derek | Level 1 | ISQ735 |
|Jensen, Jeff | Level 1 | ISQ700 |
|Johnson, Brian | Level 1 | ISQ620 |
|Jolly, Ben | Level 1 | ISQ720 |
|Kesler, Thomas | Level 1 | ISQ620 |
|LaBarge, Timothy | Level 1 | ISQ600 |
|Manguiat, Jennifer^ | Level 1 | ISQ740 |
|Narez, Alfredo | Level 1 | ISQ735 |
|Oney, Dana | Level 1 | ISQ720 |
|Pickard, Katie | Level 1 | ISQ540 |
|Richardson, John | Level 1 | ISQ621 |
|Rigakos, Peter | Level 1 | ISQ620 |
|Ripley, Susan | Level 1 | ISQ735 |
|Robinson, Jason^ | Level 1 | ISQ740 |
|Rosenfeldt, Fonda | Level 1 | ISQ501 |
|Royster, John | Level 1 | ISQ621 |
|Stinson, Morgan | Level 1 | ISQ501 |
|Testa, Steve | Level 1 | ISQ700 |
|Wagoner, Mike | Level 1 | ISQ641 |
|Busch, Michelle | Level 1* | ISQ621 |
|Chapman, Hunter | Level 1* | ISQ621 |
|Hogan, Amanda | Level 1* | ISQ621 |
|Hendricks, John | Level 1* | ISQ631 |
|Hewitt, Christina | Level 1* | ISQ621 |
|McEntire, Monica | Level 1* | ISQ621 |
|Murphy, Kelly | Level 1* | ISQ631 |
|Vitatoe, Shane | Level 1* | ISQ621 |


    

