title: Data Governance        
author: Ben Jolly        
published: 2020-12-10        
data classification: Internal    
approver(s):  Buck Brody, Bob Ven   
status: draft    


# STATEMENT OF PURPOSE  

To support effective management, company data must be accessible, must correctly represent the information intended, and must be easily integrated across corporate information systems. The purpose of data governance is to develop company-wide policies and procedures that ensure that company data meet these criteria within and across the company’s administrative data systems.  

The purpose of the current Data Governance Policy is to achieve the following  

- Establish appropriate responsibility for the management of company data as an asset.  
- Improve ease of access and ensure that once data is located, users have enough information about the data to interpret them correctly and consistently.  
- Improve the security of the data, including confidentiality and protection from loss.  
- Improve the integrity of the data, resulting in greater accuracy, timeliness, and quality of information for decision-making.  

The Data Governance Policy addresses data governance structure and includes policies on data access, data usage, and data integrity and integration.  

# GROUPS AFFECTED BY THIS POLICY  

Any person, system, or group that creates data, access data, manages data, or relies on data for decision making and planning.  

# WHO SHOULD READ THIS POLICY  

Data governance executive sponsors, data stewards, and all other employees who use data, regardless of the form of storage or presentation.  

# DATA GOVERNANCE COMMITTEE  

- ELT Sponsor\Sponsors – Jennifer Johnson, Todd Ciganek  
    - ELT members who have planning and policy responsibility and accountability for major administrative data systems (e.g., customer, sales, marketing, human resources, and financial) within their functional areas. By understanding the planning needs of the company, they are able to anticipate how data will be used to meet company needs.  
- Data Governance Leader – Ben Jolly    
    - The Data Governance Leader works to ensure that all company data is represented within a single logical data model that will be the source for all physical data models. Informed by the Data Governance Committee, the Data Governance Leader is responsible for developing a company data model, corresponding data structures, and data domains.  
    - This individual is responsible for coordinating data policies and procedures in the three primary enterprise data systems - customer, finance, and human resources - ensuring representation of the interests of data stewards, managers, and key users. The Data Governance Leader coordinates the meetings and agendas for the executive sponsors and Data Governance Committee and provides support to related data management efforts.
- Data Stewards – TBD (Product, BO, Finance, Marketing, Jeff Cryder, HR, Content, Dave S.)  
    - Data stewards are appointed by executive sponsors to implement established data policies and general administrative data security policies. Data stewards, who comprise the Data Governance Committee, are responsible for safeguarding data from unauthorized access and abuse through established procedures. They authorize the use of data within their functional areas and monitor this use to verify appropriate data access. They support access by providing appropriate documentation and training to support company data users. Included among data stewards are the following personnel currently in place at ConstructConnect: SHOULD BE COMPOSED OF HR, FINANCE, PRODUCT, BACK OFFICE, IT, ETC  
- Data Administrators  
    - Data administrators are company employees who most often report to data stewards and whose duties provide them with an intricate understanding of the data in their area. They work with the data stewards to establish procedures for the responsible management of data, including data entry and reporting. Some data administrators may work in a technology unit outside of the functional unit, but have responsibilities for implementing the decisions of the stewards.  
    - Technical data administrators may be responsible for implementing backup and retention plans, or ensuring proper performance of database software and hardware.  

# TABLE OF CONTENTS

1. Data Governance Structure  
1. Data Management Policy  
1. Data Usage Policy 
1. Data Integrity and Integration  

## Data Governance Structure 

Data Governance is the practice of making strategic and effective decisions regarding the organization’s data and information assets. It assumes a responsibility to adhere to all policies and all legal constraints that govern the proper use and management of data and information by all members of the enterprise’s community, including but not limited to employees, contractors, and external partners.  

So the company can achieve effective Data Governance, the company has chosen to apply formal guidelines to manage the enterprise’s data and information assets, and will assign staff to implement them.  

The Data Governance Committee is a group, led by the Data Governance Leader and supported by the ELT.  The Data Governance Committee will appoint data stewards, and through the establishment of data policies and organizational priorities, provide direction to them and data administrators.  The Data Governance Committee is a body that meets regularly to address a variety of data issues and concerns.   

## Data Management Policy  
Security and Access

The purpose of the data access policy is to ensure that employees have appropriate access to organizational data and information and that the data is secure.  While recognizing the company’s responsibility for the security of data, the procedures established to protect that data must not interfere unduly with the efficient conduct of the organization’s business.  This policy applies to all business units and to all uses of company data, regardless of the offices or format in which the data reside.  

The policy will protect its data assets through security measures that assure the proper use of the data when accessed.  Every data item will be classified by the relevant data steward to have an appropriate access level.  Data Management will be conducted in accordance with the policies established by the organization.  

Any employee or non-employee denied access to data may appeal the denial to the Data Governance Committee; decision is final from the DG Committee.  

See complete [Data Management Policy](/pages/policy/Construct%20Connect%20Policy%20Documentation/Information%20Security/Data%20Management%20Policy)

## Data Usage Policy  
Acceptable Use Policy  

The purpose of the data usage policy is to ensure that company data are not misused or abused, and are used ethically, according to any applicable law, and with due consideration for individual privacy.  Use of data depends on the security levels assigned by the relevant business data steward.  

Personnel must access and use data only as required for the performance of their job functions, not for personal gain or for other inappropriate purposes; they must also access and use data according to the security levels assigned to the data.  Data usage falls into the categories of create, update, read-only, and external dissemination.  

Authority to update data shall be granted by the appropriate data steward only to personnel whose job duties specify and require responsibility for data update.  This restriction is not to be interpreted as a mandate to limit update authority to members of any specific group or office but should be tempered with the company’s desire to provide excellent service to employees, customers / members, representatives, and other constituents.  Company employees, contractors and business partners who fail to comply with the data usage policy will be considered in violation of the relevant ConstructConnect codes of conduct and may be subject to disciplinary action or to legal action if laws have been violated.  In less serious cases, failure to comply with this policy could result in denial of access to data.  

See complete [Acceptable Use Policy](/pages/policy/Construct%20Connect%20Policy%20Documentation/Information%20Security/Acceptable%20Use%20Policy)  

## Data Integrity and Integration  

The purpose of this is to ensure that the organization’s data have a high degree of integrity and that key data elements can be integrated across functional units and electronic systems so that staff, contractors and management may rely on data for information and decision support.  

Data integrity refers to the validity, reliability, and accuracy of data.  Data integrity relies on a clear understanding of the business processes underlying the data and the consistent definition of each data element. 

Data integration, or the ability of data to be assimilated across information systems, is contingent upon the integrity of data and the development of a data model, corresponding data structures, and domains.  

Data must be defined as data that are maintained in support of a functional unit’s operation and meet one or more of the following criteria  

- the data elements are key fields, that is, integration of information requires the data element  
- the organization must ensure the integrity of the data to comply with internal and external administrative reporting requirements, including enterprise planning efforts  
- the data are reported on or used in official / regulatory reports  
- a broad cross section of users requires the data  

It is the responsibility of each data steward, in conjunction with the Data Governance Committee and the Data Governance Leader, to determine which core data elements are part of the organization’s essential/core data.  

Data governance thought processes should consider ability to avoid vendor lock in where possible. In general, the more custom code in an application makes it harder to transition to a new application if needed. Transition time should be considered when doing vendor selection or building data processes to ensure we are as agile as possible in our data strategy. For example, if we eliminated Salesforce and moved to a new CRM, it would likely still contain things like accounts, contacts and opportunities. More complicated business rules could be recreated, but ideally are as minimal as necessary. For our own custom platform logging, things like paid status should be maintained internally versus logged in a third party application. If we had to leave that application, it could create significant rework on business rules that aren't easily transitioned from one application to the next. Those core data attributes for our business should be maintained internally when possible. In general, when custom code is being considered in applications, the ability to transition vendors should be considered.  

Documentation (metadata) on each data element will be maintained within a corporate repository according to specifications provided by the Data Governance Committee. These specifications will include both the technical metadata and definition of each element, as well as a complete interpretation that explains the meaning of the element and how it is derived and used.  The interpretation will include acceptable values for each element, and any special considerations, such as the values that determine whether a person is a member or a customer.  

All employees are expected to bring data problems and suggestions for improvements to the attention of the appropriate data stewards, the Data Governance Committee, or the Data Governance Leader.  

In order to maintain Data Integrity across Construct Connect everyone should familiarize themselves with the [Data Standards Policy]() COMING SOON and Standard Business Rules for their team.  

Contacts  
ELT Sponsor(s)  
<jennifer.johnson@constructconnect.com>  
<todd.ciganek@constructconnect.com>  

Data Governance Leader  
<ben.jolly@constructconnect.com>  
