title: Navigating Purchase Policies
author: Dave Storer, Controller
published: 2020-01-13


Please see the [Delegation of Authority](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Delegation%20of%20Authority%20-%20ConstructConnect) for approval levels by individual.For any questions, please contact <accountspayable@constructconnect.com>.  
Note that certain navigation blocks contain links to associated policies or reference files.


<div class="mermaid">
graph LR

A[Vendor Purchase] -->|small one off|T["Travel, Gifts, Under $1k"]
TP --> C[Use P Card or Reimburse] --> Concur
T --> TP[Non-PO Policy]
A --> ROP[Lease or Charitable]
A --> FormerEmployee --> DOA
A --> CapEx --> DOA
ROP --> DOA[DofA]
A --> S[Software or Consulting] --> SP[SoftwarePolicy]
S --> ConsultingPolicy --> R[BizCentral]
SP -->R
DOA --> R
A --> AO[All Other] --> R
R --> REQ[Requistion]
REQ --> ApprovedPO
ApprovedPO --> Sign
Sign --> Buy

subgraph Type
ROP
S
AO
T
CapEx
FormerEmployee 
end

subgraph Governing Policy
ConsultingPolicy
SP
DOA
TP
end

click ConsultingPolicy "/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Consulting%20and%20Professional%20Services%20Purchasing"

click SP "/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Software%20Purchasing"

click DOA "/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Delegation%20of%20Authority"

click TP "/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Travel%20and%20Entertainment"

click R "https://businesscentral.dynamics.com"

click Concur "https://www.concursolutions.com/nui/signin/v2"

</div>


