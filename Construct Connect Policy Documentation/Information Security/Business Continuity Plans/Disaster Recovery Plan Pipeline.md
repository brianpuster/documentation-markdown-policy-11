title: Business Continuity Plan For Pipeline
author: VerSprite vCISO
published: 2020-06-09


#### Policy name:
Disaster Recovery Plan Pipeline

#### Policy level:
Annual General

#### Data Classification:
Confidential

#### Author:
VerSprite vCISO

#### Approver(s)
CTO (Bob Ven)  
EVP Finance (Buck Brody)

#### Location or Team Applicability
All Locations, IT Team

#### Effective date:
input date using YYYY-MM-DD format



[Disaster Recovery Plan](https://teams.microsoft.com/_#/files/BCP%20PLans?threadId=19%3A6906a73b2484401885a11efdfb63341b%40thread.skype&ctx=channel&context=BCP%2520PLans&rootfolder=%252Fsites%252Fitinfrastructure%252FShared%2520Documents%252FBCP%2520PLans)

