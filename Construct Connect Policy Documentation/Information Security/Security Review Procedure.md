title: Security Review Procedure  
author: VerSprite vCISO  
published: 2020-10-15  
policy level: Annual General  
data classification: Internal  
approver(s): Buck Brody, EVP Finance; Bob Ven, CTO; Zandy McAllister, Security Operations Manager; Dana Oney, Director of IT Ops  
location or team applicability: All locations, all teams  


# GENERAL

## Introduction

This procedure establishes when ConstructConnect employees need to contact the InfoSec Team via [securityreviews@constructconnect.com](mailto:securityreviews@constructconnect.com), and describes roles and responsibilities in responding.

## Scope

This document applies to all information and information systems at ConstructConnect and all employees, contractors, vendors, and service providers who have access to ConstructConnect information or information systems.

## Objectives

The objective of this procedure is to assist ConstructConnect employees, contractors, vendors, and service providers to identify when and how to contact the InfoSec team via [securityreviews@constructconnect.com](mailto:securityreviews@constructconnect.com). The primary objectives of this procedure are:

- Establish conditions that, if met, require contacting the InfoSec Team;
- Provide instructions on what information to include in the initial communication to the InfoSec Team;
- Instruct the InfoSec team on how to process and prioritize responses and communicate with effected personnel.

## Responsibilities

**All Company Personnel** are responsible for communicating with the InfoSec Team via [securityreviews@constructconnect.com](mailto:securityreviews@constructconnect.com) whenever one or more of the conditions are met.

**InfoSec Team** is responsible for:

- Timely review of submissions,
- Triaging submissions and identifying additional information,
- Requesting additional necessary information through e-mail, questionnaires, and/or meetings,
- Providing a timeline for completion of the Security Review.

**Managers** are responsible for ensuring that personnel under their purview act in accordance with this policy.

## Review

This document must be reviewed for updates annually or whenever there is a material change in ConstructConnect’s business practices that may reasonably implicate the security, confidentiality, integrity or availability of data and systems. The CISO owns this Procedure, but any update or change must be approved by the Risk Committee.

# When to Request a Security Review

Security Review requests can come from various sources, either by one’s own observations or by notification from other sources. Security Reviews do not substitute Change Requests or the Change Management Policy. All Security Review requests will be reviewed by the InfoSec Team or CISO in a timely manner.

A Security Review request should be sent to [securityreviews@constructconnect.com](mailto:securityreviews@constructconnect.com) whenever a proposed vendor, service, or Change (as defined by the Change Management Policy):

- Grants or changes the ability to store, process, or transmit data. For example, installation of software, browser extensions or toolbars;
- Has the potential to disrupt a defined Business Process (listed on [guidebook](/pages/policy));
- Grants or escalates any privileges on devices;
- Changes the function or access of network security. For example:

    - Firewall settings;
    - Allow-lists or Deny-lists;
    - Logging and/or auditing; and/or
    - Granting or escalating an external party’s access to the corporate network, private network (including VPN);

- Changes the function of endpoint security. For example:

    - Port opening/closing;
    - Allow-lists or Deny-lists;
    - Permitted or prohibited services;
    - Encryption protocols;
    - Anti-malware agents;
    - Logging and/or auditing;
    - Data Loss Prevention (DLP); and/or
    - Web filtering rules.

- Security questionnaire(s) from clients or potential clients;
- Security bug report(s);
- Security event reports;
- Request for any rapid risk assessment; and
- Security questions in general.

# How to Request a Security Review

A Security Review request should be sent to [securityreviews@constructconnect.com](mailto:securityreviews@constructconnect.com) with at least the following information (please copy and paste this list and respond to each item when submitting a Security Review request):

- Identification of the Requestor who submitted the Security Review Request;
- Identification of the proposed vendor, service, or Change;
- Identification of the business issue or need related to the proposal, i.e. “What problem is the proposal meant to solve?”;
- Description of how the proposal will address the issue or need;
- Listing of alternate vendors, services, or Changes that were also considered;
- Reasoning for selecting this proposed vendor, service, or Change;
- Listing of existing systems impacted;
- Listing of data the solution or vendor will have access to;
- Listing of existing Business Processes impacted;
- Diagramming data flow for any Personal Information related to the proposed vendor, service, or Change; and
- Estimation of timeline for implementation.

## Prioritizing Security Reviews

Once a Security Review request is received by the InfoSec Team and CISO, the request must be documented in the approved task tracking location and prioritized based on scale and classification of data impacted, security risk, and criticality of related business processes. For example:

- Does the Security Review request relate to Internal data (lower), or Sensitive/Confidential data (higher)?
- Does the Security Review request relate to no known security risks (lower), or known and serious security risks (higher)?
- Does the Security Review request relate to a Tier 4 or Tier 3 business process (lower), or to a Tier 2 or Tier 1 business process (higher)?

Once prioritized, the InfoSec Team or CISO must communicate with the requestor regarding the timeline for delivery and identifying any additional information or follow-ups needed to complete the Security Review. This communication should include a statement that the Security Review will not begin until the follow-ups have been provided. These follow-ups may include, at the InfoSec Team’s discretion, specific questions, questionnaires, and/or meetings.

## Documentation

A Security Review shall be documented using a pre-approved form and provide at least the following:

- A data classification tag of “Confidential” displayed on every page;
- Identification of the proposed vendor, service, or Change;
- The use cases related to that proposal;
- The Tier 1 and Tier 2 business processes potentially impacted by this vendor, service, or Change
- Potential implications on compliance with Payment Card Industry (PCI), Sarbanes-Oxley (SOX), or Privacy (CCPA, PIPEDA);
- Inherent Risk related to the proposal;
- Recommended controls to reduce inherent risk;
- Residual Risk once recommended controls have been implemented; and
- A recommendation from the InfoSec Team or CISO either in favor or in opposition of adopting the proposal.

Once complete, the documented Security Review shall be submitted to the requestor in .pdf format.

## Security Review Dispute

Once a Security Review is received by the requestor, the requestor has the option to dispute the findings and recommendations of the Security Review. Any dispute must be submitted to [securityreviews@constructconnect.com](mailto:securityreviews@constructconnect.com) in writing and specifically identify the disputed finding(s) and/or recommendation(s) and reason for the dispute. All disputes shall be reviewed by the InfoSec Team or CISO. The InfoSec Team or CISO shall respond to the dispute in writing in a timely manner.

## Exceptions

Exceptions to this Procedure are not permitted unless properly documented and submitted via the Exceptions Procedure and have the required management approval.

# References

## Relevant Regulations

- PCI-DSS
- US Data Privacy laws
- Sarbanes-Oxley

## Relevant NIST CSF Domain(s)/Category(ies)/Subcategory(ies)

- Identify
- Protect
- Detect

## Relevant Roper CIS Domain(s)/Category(ies)/Subcategory(ies)

- 1 - Cybersecurity Governance and Risk Management
- 2 - Inventory and Asset Management
- 4 - Access Management
- 5 - Configuration Management
- 6 - Audit Logging and Monitoring
- 7 - Physical and Environmental Controls
- 8 - Email, Malware and Web Protection
- 9 - Network Security
- 11 - Data Protection
- 12 - Segregation of Duties
- 13 - Supply Chain Risk Management
- 15 - Secure System Development - SDLC

## Related Policies, Plans, Procedures and Guidelines

- Access Control Policy
- Audit Logging and Monitoring Policy
- Change Management Policy
- Data Management Policy
- Personnel Security Policy
- Physical and Environmental Security Policy
- Risk Management Policy
- Secure Development Policy

## Insert any procedure or process documentation (optional):
