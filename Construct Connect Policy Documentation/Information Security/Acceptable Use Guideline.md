title: Acceptable Use Guideline  
author: ConstructConnect People and Culture, adapted by VerSprite vCISO  
published: 2021-04-16          
policy level: Annual General         
data classification: Internal        
approver(s): Buck Brody, EVP Finance; Versprite CISO; Dana Oney, Director of IT Ops; Jennifer Manguiat, Director People & Culture      
location or team applicability: All locations, all teams      


# GENERAL

The term “Company Computer Systems” in context of this policy includes Company-provided internet, instant messaging, common drives, e-mail, phone systems, and all related hardware and software. This policy applies to all types of communication activities utilizing Company owned computers, communication equipment, systems and networks.

## Objective

The purpose of this document is provide a more comprehensive list of activities that either can or should not be done using ConstructConnect Information Technology resources, including company-provided internet, instant messaging, common drives, e-mail, phone systems, and all related hardware and software.

## Review

This document must be reviewed for updates annually or whenever there is a material change in the Company's business practices that may reasonably implicate the security, confidentiality, integrity or availability of data and systems. The CISO owns this Policy, but any update or change must be approved by the Risk Committee.

## Team Member Usage Expectations 

Access to the Company’s IT resources is a privilege granted to all users to support their job duties and to support the goals and objectives of the Company.  
 
The Company expects that team members will:

- Login to the system using the identification and authentication provided to them (unique user ID and password) which may be changed by the team member at a later date.

- Operate computer equipment for the performance of their official duties only during working time. The use of computer equipment for personal or non-business purposes should be limited to non-working time, and inappropriate use may result in disciplinary actions against the team member, as deemed appropriate by management.

- Conduct themselves honestly and appropriately while using computer equipment, and respect the copyrights, software licensing rules, property rights and privacy of others.

- Take proper care of all company equipment that the team member is entrusted with.  Upon leaving the Company a team member will return all company equipment in proper working order. Failure to return equipment will be considered theft and will lead to criminal prosecution by the Company. 

The Company requires that team members will not:

- Attempt to disable, defeat or circumvent any Company security feature or mechanism.

- Access, archive, store, distribute, edit, print, display, or record any kind of sexually explicit, threatening, demeaning, harassing, offensive or otherwise inappropriate software or electronic files (images, documents, videos, etc.).

- Attempt without prior authorization to alter the content, structure or functionality of Company-supplied facilities, i.e., content of the web site, sequence of presentation, routing and/or linking to other sites or addresses, etc.

- Attach personal devices to Company equipment (e.g. USB drives, personal music players, personal computers, laptops and/or games) is prohibited. 

- Use Company resources to operate a privately-owned business, solicit funds, or promote religious beliefs to others.

- Intentionally or carelessly perform an act that will place an excessive load on a computer or network to the extent that other users may experience denied or disrupted service.

- Use the Company’s logo, trademark or proprietary graphics for any commercial purpose, without authorization, or in a manner that suggests you are representing the Company, or while engaging in activity that is unlawful or violates Company policy.

- Download or store confidential Company or customer information is prohibited.

- Download any software or electronic files without implementing virus protection measures that have been approved by the Company.

- Knowingly violate the laws and regulations of the United States or any other nation, or the laws and regulations of any state, city, or other local jurisdiction. 
 
Use of any of these resources for illegal activity may be grounds for discipline up to and including immediate termination.

## Internet Access

Anyone identified or identifiable as a team member is considered a representative of the Company and must behave accordingly, including while on multi-media, social networking, blogs and wiki sites for both professional and personal use. The following guidelines should be followed.  

- Team members inadvertently connected to an Internet site that contains offensive or sexually explicit material, must disconnect from that site immediately and notify their manager.

- Team members may access social media sites for personal use during break periods and lunch breaks only unless directly related to a job requirement per the Company computer usage policy.

- If being on social media sites is part of your job responsibilities, then you must clearly identify yourself as an ConstructConnect team member in your postings or blog site(s) and include a disclaimer that the views are your own and not those of ConstructConnect unless you are authorized in writing by your manager to do so. You are not permitted to post comments in the name of the employer or in a manner that could reasonably be attributed to the employer, without prior written authorization.

- ConstructConnect team members should not circulate postings they know are written by other ConstructConnect team members without informing the recipient that the author of the posting is a ConstructConnect team member.

- Because you are legally responsible for your postings, you may be subject to liability if your posts are found defamatory, harassing, threatening, intimidating, bullying, consist of discriminatory comments, slanderous comments, maliciously false comments, or in violation of any other applicable law.

- Your Internet postings should respect copyright, privacy, fair use, financial disclosure, and other applicable laws. Your postings should not contain proprietary, trade secret or attorney-client privileged information.

- You may also be liable if you make postings which include proprietary or copyrighted information (music, videos, text, etc.) belonging to third parties. All of the above-mentioned postings are prohibited under this policy.

- Team members with Internet access may not use Company resources to download and/or play entertainment software, music or games. 

- Downloading streaming video (e.g., watching movies through the Internet) or to participate in newsgroups with automatic update features (e.g., Pod cast) is not permitted, unless the use is directly related to a job requirement.

- While at work, creating and responding to blogs, wikis, and other forms of personal online discourse must be restricted to work-related requirements.  

- All the Company rules and policies regarding confidentiality and proprietary information apply to cyberspace commentary or blog activities.  Team members are personally responsible and liable for the content of information posted to cyberspace commentary or blogs.  

- Information communicated on any social media site that violates the Company policies or constitutes harassment of co-workers; even during off hour activities and/or on a third-party site is a serious offense and will be subject to disciplinary action, up to and including termination of employment.

## E-mail Management and Instant Messaging (IM)

- Use of Company e-mail is for business purposes and limited personal usage only.  E-mail correspondence is not private, and the Company can and will access and read any and all information contained in computers, computer files, e-mail messages or voice messages.  Team members should have no expectation of privacy with regard to these communications and will be in violation of the Company’s discrimination and harassment policy if they send, receive or access discriminatory, harassing or otherwise inappropriate e-mails or voice mails. The content of e-mail messages is admissible as evidence and may be subject to subpoena in discovery.  

- Do not send or forward e-mails that contain libelous, defamatory, offensive, harassing, explicit, racist, or obscene remarks.

- Do not send e-mails to entire office or Company without first obtaining written authorization from the Chief People Officer.

- Do not send work containing confidential client information from an office computer to a home computer via e-mail.  Company-related e-mail must not be stored or transferred to non-work-related computers.

- Instant messaging is to be used for business purposes only, and it is expected that users will communicate professionally at all times.  The Company can and will monitor, inspect, copy, review, store and audit IM usage. 

- Transmitting confidential information through IM is forbidden. Credit card numbers are not to be transmitted or stored via email or IM/chat.

## Harassment

Anyone receiving any kind of sexually explicit, threatening, demeaning, harassing, offensive or otherwise inappropriate image on any Company computer system or electronic communications system should retain the document, correspondence or file and immediately notify their manager.

## Passwords and Locking/Logging Off

- The responsibility for protecting authentication credentials to systems and applications falls upon the individual to whom those provisions were originally provided. Sharing of user accounts is prohibited. Employees should never give their password to anyone.  The user ID and password combination is confidential and therefore: 

    - Must not be shared with any other person, including administrators and supervisors. This includes passwords to non-guest Wi-Fi.
    
    - Must not be written down on any material so that another individual can access it.  Periodic walkthroughs should be performed to ensure compliance.
    
    - Must not be stored in an electronic format on the computer or email unless properly restricted through encryption or secured password vault.

- Users are expected to lock their computers when walking away from their office or desk. This includes workstations, laptops, mobile devices and servers. Although settings may be enabled to enforce automatic lockout and timeouts, the stronger control resides with users locking their devices as soon as they step away.  Users are responsible for actions taken using their device(s), including those performed by the adversary.
    
    - Press the Windows Key + L to lock your Windows Computer
    
    - Press Ctrl + Shift + Eject/Power to lock your Mac Computer

- When leaving work at night or over the weekend “Log off”.  Do not use the “Shut Down” option as periodic maintenance is sometimes performed, and computers must be powered on to receive these important updates.

## User Privacy

Users should have no expectation of privacy when using information systems and mobile devices above what is required by local laws and regulations.  To manage systems and enforce security, the organization may log, review and otherwise utilize any information stored on or passing through its systems.  User activity may be captured such as telephone numbers dialed and websites visited.
 
Messages sent over computer and communications systems are the property of the company.  Management reserves the right to examine all data stored in or transmitted by these systems.  
 
The users should seek assistance from the IT Security personnel to ensure the security of the transmission channel.  When providing computer-networking services, the company may not necessarily provide default message protection services, such as encryption.  No responsibility is assumed for the disclosure of information sent over networks and no assurances are made about the privacy of information handled by internal networks.  In those instances where session encryption or other special controls are required, it is the user’s responsibility to ensure that adequate security precautions are taken.

## Reporting Security Events

All workers should promptly report to IT any loss of, or severe damage to, their hardware or software. Workers should report all suspected compromises as well as all serious security vulnerabilities known to exist.  All instances of suspected disclosure of confidential information also should be reported.

- To report incidents you can enter a ticket in the SysAid ticket system, send an email to servicedelivery@constructconnect.com or contact the Service Desk at 888-202-4490

## Cloud Storage & File Transfers

Cloud storage solutions and file transfer software (i.e., Dropbox, box.com, etc.) are not allowed for storage or transmission of any confidential company data unless the method is approved by the local security team, as listed below.  Storage of confidential data in unsecure locations can represent a significant exposure risk to the organization.

## Data Sensitivity

Customer data and other proprietary information on all Company computers, networks and databases are owned by the Company and is a confidential asset governed by Federal, State and Local laws and confidentiality rules and regulations.  Team members are expected to value and respect the confidential and sensitivity of this information and data.  Any violations will be grounds for immediate termination.


## Data Classification

Data is classified into four basic categories: Confidential, Sensitive, Internal, and Public.  More information about the Data Classifications can be found in the Data Management Policy [here](https://policydocs.buildone.co/pages/policy/Construct%20Connect%20Policy%20Documentation/Information%20Security/Data%20Management%20Policy) If you have questions about which category a document should fall in please speak with your manager or contact the IT Security team by submitting a SysAid ticket with information on the document in question. 


## Remote and Wireless Access

- Approved users may access the Company network remotely through the use of a virtual private network (VPN) connection. This access can be approved by the user’s team manager and a ticket should be submitted via SysAid for access.

- Log off remote sessions immediately after completing work and do not leave computers unattended while remote sessions are active.

- When wireless access is established via an externally-supplied access point (AP), the team member is responsible for applying the highest level of security available for that AP. Confidential data is not to be sent/received over any wireless connections without strong encryption.   
 
Because of the evolving nature of technology, this policy is not intended to be comprehensive, and conduct not specifically mentioned that violates the spirit of this policy may also, at our discretion, result in corrective action up to and including termination.
 
Violations of any part of this policy can lead to corrective action up to and including termination.  The Company will cooperate with legal authorities in investigations of any potential illegal violations regarding e-mail and/or Internet and/or cyberspace use.

## Social Media

At the Company, we understand that social media can be a fun and rewarding way to share your life and opinions with family, friends and co-workers around the world.  However, use of social media also presents certain risks and carries with it certain responsibilities.  To assist you in making responsible decisions about your use of social media, the Company has established these guidelines for appropriate use of social media. 
 
In the rapidly expanding world of electronic communications, social media can mean many things.  Social media includes all means of communicating or posting information or content of any sort on the Internet, including to your own or someone else’s web log or blog, journal or diary, personal web site, social networking and affinity web site, web bulletin board or chat room, whether or not associated or affiliated with the Company, as well as any other form of electronic communication.
 
The same principles and guidelines found in all our Company policies apply to your activities online.  Ultimately, you are solely responsible for what you post online.  Before creating online content, consider some of the risks and rewards that are involved.  Keep in mind that any of your conduct that adversely affects your job performance, the performance of fellow team members or otherwise adversely affects clients or people who work on behalf of the Company or the Company’s legitimate business interests may result in disciplinary action up to and including termination.
 
### Know and follow the rules

Carefully read these guidelines, the Company’s policy on use of Company Electronic Resources and the Company’s policy prohibiting unlawful discrimination & harassment, and ensure your postings are consistent with these policies.  Postings that include discriminatory remarks, harassment, and threats of violence or similar inappropriate or unlawful conduct will not be tolerated and may subject you to disciplinary action up to and including termination.

### Be respectful

Always be fair and courteous to customers, vendors and suppliers.  Also, keep in mind that you are more likely to resolve work-related complaints by speaking directly with your co-workers or by utilizing our Open-Door Policy than by posting complaints to a social media outlet.  Nevertheless, if you decide to post complaints or criticism, avoid using statements, photographs, video or audio that reasonably could be viewed as malicious, obscene, threatening or intimidating, that disparage customers, vendors, suppliers or members of the public, or that might constitute harassment or bullying.  Examples of such conduct might include offensive posts meant to intentionally harm someone’s reputation or posts that could contribute to a hostile work environment on the basis of race, sex, disability, religion or any other status protected by law or Company policy.

### Avoid Posting Information You Know to be False

Always strive to be honest and accurate when posting information or news, and if you make a mistake, correct it quickly.  Be open about any previous posts you have altered. Remember that the Internet archives almost everything; therefore, even deleted postings can be searched.  Never post any information or rumors that you know to be false about the Company, fellow associates, customers, vendors, suppliers or members of the public.

### Maintain confidentiality 

Maintain the confidentiality of Confidential Information, as that term is described elsewhere in this Handbook, and do not disclose non-public customer, vendor or supplier information.  Do not create a link from your blog, website or other social networking site to a Company website without identifying yourself as a Company team member.

### Express only your personal opinions

Express only your personal opinions.  Never represent yourself as a spokesperson for the Company.  If the Company is a subject of the content you are creating, be clear and open about the fact that you are a team member and make it clear that your views do not represent those of the Company, fellow associates, members, clients, suppliers or people working on behalf of the Company.  If you do publish a blog or post online related to the work you do or subjects associated with the Company, make it clear that you are not speaking on behalf of the Company.  It is best to include a disclaimer such as “The postings on this site are my own and do not necessarily reflect the views of the Company.”

### Using social media at work

Refrain from using social media while on working time unless it is work-related as authorized by your manager.  Do not use the Company email addresses to register on social networks, blogs or other online tools utilized for personal use.

### Retaliation is prohibited

The Company prohibits taking negative action against any team member for reporting a possible deviation from this policy or for cooperating in an investigation.  Any team member who retaliates against another team member for reporting a possible deviation from this policy or for cooperating in an investigation will be subject to disciplinary action, up to and including termination.

# REFERENCES

## Regulations.

- PCI-DSS

- US Data Privacy Laws

## NIST CSF

- Identify

- Protect

## Roper CIS Controls  

- 1 Cybersecurity Governance and Risk Management  
- 2 Inventory and Asset Management  
- 3 Access Management  
- 4 Configuration Management  
- 5 Audit Logging and Monitoring  
- 6 Physical and Environmental Controls  
- 7 Email, Malware and Web Protection  
- 8 Data Protection  
- 9 Segregation of Duties  
- 10 Security Awareness  


## Related Policies, Plans, Procedures and Guidelines

- (Written Information Security Program)[https://policydocs.buildone.co/pages/policy/Construct%20Connect%20Policy%20Documentation/Information%20Security/Written%20Information%20Security%20Program]

- Policies
    
    - (Access Control Policy)[https://policydocs.buildone.co/pages/policy/Construct%20Connect%20Policy%20Documentation/Information%20Security/Access%20Control%20Policy]
    
    - (Acceptable Use Policy)[https://policydocs.buildone.co/pages/policy/Construct%20Connect%20Policy%20Documentation/Information%20Security/Acceptable%20Use%20Policy]
    
    - (Logging and Auditing Policy)[https://policydocs.buildone.co/pages/policy/Construct%20Connect%20Policy%20Documentation/Information%20Security/Logging%20and%20Auditing%20Policy]
    
    - (Change Management Policy)[https://policydocs.buildone.co/pages/policy/Construct%20Connect%20Policy%20Documentation/Information%20Security/Change%20Management%20Policy]
    
    - (Data Management Policy)[https://policydocs.buildone.co/pages/policy/Construct%20Connect%20Policy%20Documentation/Information%20Security/Data%20Management%20Policy]
    
    - (Personnel Security Policy)[https://policydocs.buildone.co/pages/policy/Construct%20Connect%20Policy%20Documentation/Information%20Security/Personnel%20Security%20Policy]
    
    - (Physical and Environmental Security Policy)[https://policydocs.buildone.co/pages/policy/Construct%20Connect%20Policy%20Documentation/Information%20Security/Physical%20and%20Environmental%20Security%20Policy]

# Insert any procedure or process documentation (optional):
