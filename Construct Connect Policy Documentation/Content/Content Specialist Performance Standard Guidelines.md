author: Karen Cassidy  
title: Content Specialist Performance Standard Guidelines  
published: 2021-03-10  

  
Content Specialist's performance will be reviewed by Content Management monthly at the beginning of each month for the previous month's performance against the individual's established minimum monthly goals. If a Content Specialist does not meet their base goals, the performance improvement plan process will begin (please see the [Employment Handbook](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/2019%20ConstructConnect%20Employment%20Handbook){:target="_blank"} for description of this process). Content Specialists who have not yet completed the 90-day orientation period will not be subject to this process, but will be held responsible for a successful orientation period, which consists of:  
  
# Orientation Period Performance Guidelines - New Hire Government Bids Team  
  
- Satisfactory attendance - outlined in the [Employment Handbook](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/2019%20ConstructConnect%20Employment%20Handbook){:target="_blank"}  
- Attitude that is open to coaching and feedback  
- Ramp to achieve 85% of base goals @ 90 Days  
- Completion of all required training sessions  
  
# Orientation Period Performance Guidelines - Promotion from Government Bids Team to Private Team  
  
- Satisfactory attendance - outlined in the [Employment Handbook](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/2019%20ConstructConnect%20Employment%20Handbook){:target="_blank"}  
- Attitude that is open to coaching and feedback  
- Ramp to achieve 85% of base goals @ 90 Days  
- Completion of all required training sessions  
  
# Orientation Period Performance Guidelines - New Hire Private Team  
  
- Satisfactory attendance - outlined in the [Employment Handbook](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/2019%20ConstructConnect%20Employment%20Handbook){:target="_blank"}  
- Attitude that is open to coaching and feedback  
- Ramp to achieve 80% of base goals @ 90 Days  
- Completion of all required training sessions  
  
  
*If team members are unable to demonstrate their ability to achieve a satisfactory level of performance during their orientation period, their employment could be terminated.*  
  
*Upon successful completion of the orientation period of 90 days team members will enter the “new employee” classification.*  
  
# Performance Guidelines New Employee Classification (Transfers and Gov Team Members)  
  
- Base Goal achievement below 95% for two months in a three-month period: 1st Stage Performance Improvement Plan.  
- Base Goal achievement below 95% for three of four months progress to 2nd Stage Improvement Plan.  
- While on 2nd Stage Improvement Plan you must achieve minimum 95% Base Goal. Failure to do so may result in termination of employment.  
- Stages run consecutively.  
- In order to be removed from performance improvement plan you must maintain 95% of Base Goal for two of three months. If at any time during the next 12 months performance slips below 95% of Base Goals the PIP process will pick up where it left off.  
  
# Performance Guidelines New Employee Classification (Private Team Direct Hire)  
  
- Base Goal achievement below 90% for two months in a three-month period: 1st Stage Performance Improvement Plan.  
- Base Goal achievement below 90% for three of four months progress to 2nd Stage Improvement Plan.  
- While on 2nd Stage Improvement Plan you must achieve minimum 90% Base Goal. Failure to do so may result in termination of employment.  
- Stages run consecutively.  
- In order to be removed from performance improvement plan you must maintain 90% of Base Goal for two of three months. If at any time during the next 12 months performance slips below 90% of Base Goals the PIP process will pick up where it left off.  
  
*In the 7th month of employment and beyond Content Specialists are classified as Regular Employees.*  
  
# Performance Guideline Regular Employee Classification  
  
- Base Goal achievement below 100% for two months in a three-month period: 1st Stage Performance Improvement Plan.  
- Base Goal achievement below 100% for three of four months progress to 2nd Stage Improvement Plan.  
- While on 2nd Stage Improvement Plan you must maintain a minimum of 95% base goal. Failure to do so may result in termination of employment.  
- Stages run consecutively.  
- To be removed from performance improvement plan you must maintain a minimum 100% of Base Goal for three months. If at any time during the next 12 months performance slips below target the PIP process will pick up where it left off.  
  
# General Information  
  
- Management reserves the right to bypass stages of the improvement process depending on the nature of the performance issue including but not limited to insubordination and willful misconduct.  
- Monthly Base Goals are prorated for approved absences.  
- Content Specialists on a PIP may not participate in Flexible Schedule policy and may be asked to decrease or eliminate participation in engagement projects/activities.  
  
This is a guideline for a performance improvement plan and is subject to change at the discretion of the Content Management at ConstructConnect.  
  
# Acknowledgement  
  
I have read and acknowledge the Content Specialist Performance Improvement Plan Process to be the process I will be held accountable to regarding my performance.