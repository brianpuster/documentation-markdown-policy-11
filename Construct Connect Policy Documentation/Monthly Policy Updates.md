title: Monthly Policy Releases Notes  
author: Steve Testa - VP Strategic Initiatives and Continuous Improvement  
published: 2021-02-12  
updated: 2021-04-14    


Items included are links to new policy releases and to specific sections of updates to existing policies.  In cases where updates have been across an entire policy a link to the whole policy is used. It is the responsibility of each team member to read and comply with all new and updated policies. 


# April 2021  
---   
- [Password Standard](/pages/policy/Construct%20Connect%20Policy%20Documentation/Information%20Security/Password%20Standard){:target="_blank"} - Released 2021-04-02  
       - Published Policy  

# March 2021  
---  
- [Software Purchasing](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Software%20Purchasing){:target="_blank"} - Updated 2021-03-11  
    - Added note on just putting memo in email  
---  
- [Consulting and Professional Services Purchasing](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Consulting%20and%20Professional%20Services%20Purchasing){:target="_blank"} - Updated 2021-03-11  
    - Added note on just putting memo in email  
---
- [Content Specialist Performance Standard Guidelines](/pages/policy/Construct%20Connect%20Policy%20Documentation/Content/Content%20Specialist%20Performance%20Standard%20Guidelines){:target="_blank"} - Released 2021-03-10  
    - Published Policy  
---  
- [Company Name Change](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Company%20Name%20Change#policy-process){:target="_blank"} - Updated 2021-03-05  
    - Policy Process section updated to read as - An Accounting case type will be used to update company info. Update the SFDC record with the company name change prior to submitting the case. The documents should be added to the google docs within Salesforce to support.  

# February 2021  
--- 
- [Documentation of Approvals – General Requirements](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Documentation%20of%20Approvals%20General%20Requirements) - Updated 2021-02-26  
    - Added best practice for email approvals to be saved as PDF and attached to a purchase request  
---  

- [Library Hours Guidelines](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Library%20Hours%20Guidelines){:target="_blank"} - Released 2021-02-26  
    - Published policy  

--- 
- [Customer Suspension, Cancellation and Write-off](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Customer%20Suspension,%20Cancellation%20and%20Write-off){:target="_blank"} - Updated 2021-02-24  
    - Changed the format of the request email  
---  
- [Requisition and Salary Adjustment](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Requisition%20and%20Salary%20Adjustment){:target="_blank"} - Updated 2021-02-24  
    - Significant updates. Please read full policy.  
---
- [Consulting and Professional Services Purchasing](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Consulting%20and%20Professional%20Services%20Purchasing){:target="_blank"} - Updated 2021-02-23  
    - Significant updates. Please read full policy.

---
- IT, Infastructure & Back Office - [IT Time tracking/reporting](/pages/policy/Construct%20Connect%20Policy%20Documentation/IT,%20Infastructure%20&%20Back%20Office/IT%20Time%20tracking/reporting){:target="_blank"} - Released 2021-02-10  
    - Published policy  

---
- Software Purchasing - Updated 2021-02-10  
[Software Purchasing](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Software%20Purchasing#software-purchasing)  
    - Approval Requirements
        - Prior - CTO, EVP Finance and VP of IT Operations  
        - New - EVP Finance, VP of IT Operations and, if over $100k, the CTO  
---
- TC Customer Success PIP - Updated 2021-02-03  
[Additional Metrics section](/pages/policy/Construct%20Connect%20Policy%20Documentation/Trade%20Contractor/Customer%20Success%20-%20PIP#additional-metrics){:target="_blank"} 
    - Prior - Standard - 30 dials/day equating to 180 dials/week  
    - New - Standard - 30 dials/day equating to 150 dials/week

# January 2021  
---  
- Consulting and Professional Services - Updated 2021-01-27  
[Vendor Privacy and Security Checklist section](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Consulting%20and%20Professional%20Services%20Purchasing#vendor-privacy-and-security-checklist){:target="_blank"}  
    - Vendor checklist inserted directly into file
---
- [2021 Hourly Payroll Target Dates](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/2021_Semimonthly_Pay_Sched_Hourly){:target="_blank"} - Released 2021-01-27    
- [2021 Payroll Target Dates](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/2021_Payroll_Target_Dates){:target="_blank"} - Released 2021-01-27    
- [2021 Important Dates](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/2021_Important_Dates){:target="_blank"} - Released 2021-01-27    
    - Changed last pay date from December 30, 2021 to December 31, 2021
---
- [Customer Suspension, Cancellation and Write-off](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Customer%20Suspension,%20Cancellation%20and%20Write-off){:target="_blank"} - Updated 2021-01-20    
    - Updated request format  
---
- [Sales Prospecting and Marketing Policy](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Sales%20Prospecting%20and%20Marketing){:target="_blank"}  - Reeleased 2021-01-15  
    - Published Policy  
---
- BPM Pricing and Discounting - Updated 2021-01-07    
    - [Violations section](/pages/policy/Construct%20Connect%20Policy%20Documentation/Building%20Product%20Manufacturer/Pricing%20and%20Discounting#violations-to-the-pricing-and-discounting-policy-will-be-handled-as-follows){:target="_blank"}   
- General Contractor Pricing and Discounting - Updated 2021-01-07    
    - [Violations section](/pages/policy/Construct%20Connect%20Policy%20Documentation/General%20Contractor/Pricing%20and%20Discounting){:target="_blank"}  
- Trade Contractor Pricing and Discounting - Updated 2021-01-07  
    - [Violations section](/pages/policy/Construct%20Connect%20Policy%20Documentation/Trade%20Contractor/Pricing%20and%20Discounting){:target="_blank"}  

    - Prior
        - 1st offense – No commission on the deal and a verbal warning
        - 2nd offense – No commission on the deal and a written warning
        - 3rd offense – No commission on the deal and a final written warning
        - 4th offense – No commission on the deal and termination       
    - New
        - 1st offense – No commission on the deal (at EVP's discretion) and a documented verbal warning  
        - 2nd offense – No commission on the deal and a Stage 1 Performance Improvement Plan  
        - 3rd offense – No commission on the deal and a State 2 Performance Improvement Plan  
        - 4th offense – No commission on the deal and Termination  
    - The Company reserves the right to alter this progression on a case-by-case basis, taking all facts into consideration, including the severity and impact of the infraction.  
---  

- [PlanSwift - BDR PIP](/pages/policy/Construct%20Connect%20Policy%20Documentation/Takeoff%20Team/PlanSwift%20-%20BDR%20PIP){:target="_blank"} - Updated 2021-01-06  
- [PlanSwift - SMB Sales Rep PIP](/pages/policy/Construct%20Connect%20Policy%20Documentation/Takeoff%20Team/PlanSwift%20-%20SMB%20Sales%20Rep){:target="_blank"} - Updated 2021-01-06  
    - Significant updates. Please read full policy.  
    
---
- [Renewal Price Uplift Policy](/pages/policy/Construct%20Connect%20Policy%20Documentation/Company%20Wide/Renewal%20Price%20Uplift){:target="_blank"} - Released 2021-01-04
    - Published Policy



  





